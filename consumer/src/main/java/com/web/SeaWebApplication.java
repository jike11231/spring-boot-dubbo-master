package com.web;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
public class SeaWebApplication {
    /*1、启动zookeeper
      2、启动Tomcat
      3、启动项目
      4、利用命令行启动telnet 192.168.153.1 20552 查看进入SeaProviderLogApplication的dubbo服务
         telnet 192.168.153.1 20553查看进入SeaProviderMessageApplication的服务
    */

    public static void main(String[] args) {
        SpringApplication.run(SeaWebApplication.class, args);
    }
}
