package com.service.impl;

import com.entity.Hello;
import com.service.HelloService;
import java.util.HashSet;
import org.apache.dubbo.config.annotation.Service;

/**
 * 短信接口
 */
@Service
public class HelloServiceImpl implements HelloService {


    @Override
    public Hello getData() {
        Hello hello = new Hello();

        HashSet<String> strings = new HashSet<>();
        strings.add("test1");
        strings.add("test2");
        strings.add("test3");

        hello.setNames(strings);
        return hello;
    }
}
