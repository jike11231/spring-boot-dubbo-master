package com.service;

/**
 * 短信接口
 */
public interface SmsService {

    String sendMsg(String msg);

    void sendMsgVoid(String msg);

}
